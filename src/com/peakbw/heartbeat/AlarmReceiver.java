package com.peakbw.heartbeat;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.telephony.TelephonyManager;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
	private String iccid,imei,batteryLevel;
	private boolean isCharging,usbCharge,acCharge;
	private static final String DEBUG_TAG ="AlarmReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		
		//get phone unique identifier 
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        imei = telephonyManager.getDeviceId();
        Log.d(DEBUG_TAG, imei);
        
        TelephonyManager telemamanger = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        iccid = telemamanger.getSimSerialNumber();
        Log.d(DEBUG_TAG, iccid);
        
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        
     // Are we charging / charged?
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||status == BatteryManager.BATTERY_STATUS_FULL;

        // How are we charging?
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
        
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;
        batteryLevel = String.valueOf(batteryPct);
		
		
		int respCode = 0;
		InputStream stream = null;
		String xmlstr = createXML();
		try{
			URL url = new URL("http://peakbw.com/sandbox/senaca/heartbeat.php");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(150000);
			conn.setConnectTimeout(20000);
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			OutputStream os = new BufferedOutputStream(conn.getOutputStream());
			Log.d(DEBUG_TAG, xmlstr);
			os.write(xmlstr.getBytes());
			os.flush();
			os.close();
			conn.connect();
			Log.d(DEBUG_TAG, "Connection open");
			respCode = conn.getResponseCode();
			Log.d(DEBUG_TAG, "RespCode = "+respCode);
			if(respCode==200){
				//MainActivity.lenghtOfFile = conn.getContentLength();
				stream = new BufferedInputStream(conn.getInputStream());
				Log.d(DEBUG_TAG, "got inputstream");
				
				int ch ;
		        StringBuffer sb = new StringBuffer();
		        while((ch = stream.read())!=-1){
		            sb.append((char)ch);
		        }
		        Log.d(DEBUG_TAG, "Reply XML= "+sb.toString());
			}
        }
		catch(IOException ex){
			
		}

	}
	
	private String createXML(){
		
		StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        //xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<method>").append("heart_beat").append("</method>");
        xmlStr.append("<imei>").append(imei).append("</imei>");
        xmlStr.append("<iccid>").append(iccid.substring(0, iccid.length()-1)).append("</iccid>");
        //xmlStr.append("<time>").append(time).append("</time>");
        xmlStr.append("<isCharging>").append(isCharging).append("</isCharging>");
        xmlStr.append("<usbCharge>").append(usbCharge).append("</usbCharge>");
        xmlStr.append("<acCharge>").append(acCharge).append("</acCharge>");
        xmlStr.append("<batteryLevel>").append(batteryLevel).append("</batteryLevel>");
        xmlStr.append("</pbtRequest>");
        Log.d(DEBUG_TAG, "Request = "+xmlStr.toString());
        return xmlStr.toString();
	}

}
