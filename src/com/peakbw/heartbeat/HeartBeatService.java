package com.peakbw.heartbeat;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

public class HeartBeatService extends Service {
	private String iccid,imei,batteryLevel;
	private boolean isCharging,usbCharge,acCharge;
	private static final String DEBUG_TAG ="HeartBeatService";
	
	Timer startTimer;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
    public void onCreate() {
          super.onCreate();
        //get phone unique identifier 
          TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
          imei = telephonyManager.getDeviceId();
          Log.d(DEBUG_TAG, imei);
          
          TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
          iccid = telemamanger.getSimSerialNumber();
          Log.d(DEBUG_TAG, iccid);
          
          IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
          Intent batteryStatus = this.registerReceiver(null, ifilter);
          
       // Are we charging / charged?
          int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
          isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||status == BatteryManager.BATTERY_STATUS_FULL;

          // How are we charging?
          int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
          usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
          acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
          
          int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
          int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

          float batteryPct = level / (float)scale;
          batteryLevel = String.valueOf(batteryPct);
          
          
          startSheduler();
    }
	
	private void startSheduler() {
		startTimer = new Timer();
		/*final Handler handler = new Handler();
		
		TimerTask doAsynchronousTask = new TimerTask() {       
	        @Override
	        public void run() {
	            handler.post(new Runnable() {
	                public void run() {       
	                    try {
	                    	makeConnection(); 
	                    } catch (Exception e) {
	                        // TODO Auto-generated catch block
	                    }
	                }
	            });
	        }
	    };
	    
	    startTimer.schedule(doAsynchronousTask, 0, 60000);*/ //execute in every 50000 ms
		
		try {
			// thread wakes up 1 min (60000) after the start of app to start
			// sending pending transactions
			startTimer.scheduleAtFixedRate(new StartTask(),0, 1 * 60000);
			// every after 2 min (120000) one pending transaction is sent to the
			// server until they are all done
		}
		catch (Exception ex) {
			
		}
	}
	
	class StartTask extends TimerTask {
		int count = 0;
		public void run() {
			count++;
			Log.d(DEBUG_TAG, "count "+count);
			if(count == 10){
				count =0;
				//Toast.makeText(getBaseContext(),"heart Beat Sent", Toast.LENGTH_LONG).show();
				makeConnection();
			}
		}
	}
	
	private void makeConnection(){
		int respCode = 0;
		InputStream stream = null;
		String xmlstr = createXML();
		try{
			URL url = new URL("http://peakbw.com/sandbox/aandm/heartbeat.php");
			Log.d(DEBUG_TAG, "url = "+url);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			Log.d(DEBUG_TAG, "connection created");
			conn.setReadTimeout(150000 );
			Log.d(DEBUG_TAG, "read time out set");
			conn.setConnectTimeout(20000 );
			Log.d(DEBUG_TAG, "conn timeout set");
			conn.setRequestMethod("POST");
			Log.d(DEBUG_TAG, "POST method set");
			conn.setDoInput(true);
			Log.d(DEBUG_TAG, "doInput = true");
			conn.setDoOutput(true);
			Log.d(DEBUG_TAG, "doOutput = true");
			OutputStream os = new BufferedOutputStream(conn.getOutputStream());
			Log.d(DEBUG_TAG, "XML = "+xmlstr);
			os.write(xmlstr.getBytes());
			os.flush();
			os.close();
			conn.connect();
			Log.d(DEBUG_TAG, "Connection open");
			respCode = conn.getResponseCode();
			Log.d(DEBUG_TAG, "RespCode = "+respCode);
			if(respCode==200){
				//MainActivity.lenghtOfFile = conn.getContentLength();
				stream = new BufferedInputStream(conn.getInputStream());
				Log.d(DEBUG_TAG, "got inputstream");
				
				int ch ;
		        StringBuffer sb = new StringBuffer();
		        while((ch = stream.read())!=-1){
		            sb.append((char)ch);
		        }
		        Log.d(DEBUG_TAG, "Reply XML= "+sb.toString());
			}
        }
		catch(IOException ex){
			ex.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	private String createXML(){
		StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        //xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<method>").append("heart_beat").append("</method>");
        xmlStr.append("<imei>").append(imei).append("</imei>");
        xmlStr.append("<iccid>").append(iccid.substring(0, iccid.length()-1)).append("</iccid>");
        //xmlStr.append("<time>").append(time).append("</time>");
        xmlStr.append("<isCharging>").append(isCharging).append("</isCharging>");
        xmlStr.append("<usbCharge>").append(usbCharge).append("</usbCharge>");
        xmlStr.append("<acCharge>").append(acCharge).append("</acCharge>");
        xmlStr.append("<batteryLevel>").append(batteryLevel).append("</batteryLevel>");
        xmlStr.append("</pbtRequest>");
        Log.d(DEBUG_TAG, "Request = "+xmlStr.toString());
        return xmlStr.toString();
	}

}
